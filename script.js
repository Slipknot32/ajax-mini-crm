$.getJSON("data/crm.json",function(data){
	
/////////////////////////////////////////////////////////////////////*      Customer 1        */

//////////////////// div description 

// variables 
	var img = data.customers[0].picture;//url
	var nom = data.customers[0].firts_name + " " + data.customers[0].last_name;
   	var date_naissance = data.customers[0].birthday;
   	var mail_num = data.customers[0].email + " " + data.customers[0].phone;
   	var company = data.customers[0].company;
   	var titre = data.customers[0].title;
 	var desc = data.customers[0].desciption; 

// appel en jquery

$("<div id='description'></div>").appendTo("#app");
	
	$("<img id='image' src='" + img + "'></img>").appendTo("#description");	
	$("<h1"+ nom + "</h1>").appendTo("#description");
	$("#description").append (nom),
   	$("<p>" + date_naissance + "</p>").appendTo("#description");
   	$("<p>"+ mail_num +"</p>").appendTo("#description");   	
   	$("<p>"+ company +"</p>").appendTo("#description");
   	$("<h2>"+ titre +"</h2>").appendTo("#description");
   	$("<p>"+ desc +"</p>").appendTo("#description");

/////////////////// div note et task 


var subject = data.customers[0].notes[0].subject;
var note = data.customers[0].notes[0].note;
var related = data.customers[0].notes[0].related_to;

var task = data.customers[0].tasks[0].task;
var category = data.customers[0].tasks[0].category;
var date = data.customers[0].tasks[0].date;
var owner = data.customers[0].tasks[0].owner;
var priority = data.customers[0].tasks[0].priority;
var status = data.customers[0].tasks[0].status;
var related_to = data.customers[0].tasks[0].related_to;
var related_deal = data.customers[0].tasks[0].related_deal;


//  appel en jquery

$("<div id='note'></div>").appendTo("#app");
    $("<p>"+ subject +"</p>").appendTo("#note")
    $("<p>"+ note +"</p>").appendTo("#note")
    $("<p>"+ related +"</p>").appendTo("#note")


$("<div id='task'></div>").appendTo("#app");

$("<ul>"+
	"<li>"+ task + "</li>"+
	"<li>"+ category + "</li>"+
	"<li>"+ date + "</li>"+
	"<li>"+ owner+ "</li>"+
	"<li>"+ priority + "</li>"+
	"<li>"+ status + "</li>"+
	"<li>"+ related_to + "</li>"+
	"<li>"+ related_deal + "</li>"+
	"</ul>").appendTo("#task");


/////////////////////// div tags 

// variables
	var tags = data.customers[0].tags;

// appel en jquery
$("<div id='tags'></div>").appendTo("#app");

	$("<p>"+ tags +"</p>").appendTo("#tags");

////////////////////////////////////////////////////////////////////////// /*      Customer 2        */

//////////////////// div description2

// variables 
	var nom = data.customers[1].firts_name + " " + data.customers[1].last_name;
	var img2 = data.customers[1].picture;//url
   	var date_naissance = data.customers[1].birthday;
   	var mail_num = data.customers[1].email + " " + data.customers[1].phone;
   	var company = data.customers[1].company;
   	var titre = data.customers[1].title;
 	var desc = data.customers[1].desciption; 

 // appel en jquery

$("<div id='description2'></div>").appendTo("#app");
	
	$("<h1"+ nom + "</h1>").appendTo("#description2");
	$("#description2").append (nom),
	$("<img id='image2' src='" + img2 + "'></img>").appendTo("#description2");	
   	$("<p>" + date_naissance + "</p>").appendTo("#description2");
   	$("<p>"+ mail_num +"</p>").appendTo("#description2");   	
   	$("<p>"+ company +"</p>").appendTo("#description2");
   	$("<h2>"+ titre +"</h2>").appendTo("#description2");
   	$("<p>"+ desc +"</p>").appendTo("#description2");

/////////////////// div task1 et task 2

	var task1 = data.customers[1].tasks[0].task;
	var category1 = data.customers[1].tasks[0].category;
	var date1 = data.customers[1].tasks[0].date;
	var owner1 = data.customers[1].tasks[0].owner;
	var priority1 = data.customers[1].tasks[0].priority;
	var status1 = data.customers[1].tasks[0].status;
	var related_to1 = data.customers[1].tasks[0].related_to;
	var related_deal1 = data.customers[1].tasks[0].related_deal;	

	var task2 = data.customers[1].tasks[1].task;
	var category2 = data.customers[1].tasks[1].category;
	var date2 = data.customers[1].tasks[1].date;
	var owner2 = data.customers[1].tasks[1].owner;
	var priority2 = data.customers[1].tasks[1].priority;
	var status2 = data.customers[1].tasks[1].status;
	var related_to2 = data.customers[1].tasks[1].related_to;
	var related_deal2 = data.customers[1].tasks[1].related_deal;


//  appel en jquery

	$("<div id='task1'></div>").appendTo("#app");	

	$("<ul>"+
		"<li>"+ task1 + "</li>"+
		"<li>"+ category1 + "</li>"+
		"<li>"+ date1 + "</li>"+
		"<li>"+ owner1 + "</li>"+
		"<li>"+ priority1 + "</li>"+
		"<li>"+ status1 + "</li>"+
		"<li>"+ related_to1 + "</li>"+
		"<li>"+ related_deal1 + "</li>"+
		"</ul>").appendTo("#task1");	

	$("<div id='task2'></div>").appendTo("#app");	

	$("<ul>"+
		"<li>"+ task2 + "</li>"+
		"<li>"+ category2 + "</li>"+
		"<li>"+ date2 + "</li>"+
		"<li>"+ owner2 + "</li>"+
		"<li>"+ priority2 + "</li>"+
		"<li>"+ status2 + "</li>"+
		"<li>"+ related_to2 + "</li>"+
		"<li>"+ related_deal2 + "</li>"+
		"</ul>").appendTo("#task2");


//////////////////////////////////////////////////////////////////////////*      Customer 3        */


// variables 
	var nom = data.customers[2].firts_name + " " + data.customers[2].last_name;
	var img3 = data.customers[2].picture;//url
   	var date_naissance = data.customers[2].birthday;
   	var mail_num = data.customers[2].email + " " + data.customers[2].phone;
   	var company = data.customers[2].company;
   	var titre = data.customers[2].title;
 	var desc = data.customers[2].desciption; 

// appel en jquery

$("<div id='description3'></div>").appendTo("#app");
	
	$("<h1"+ nom + "</h1>").appendTo("#description3");
	$("#description3").append (nom),
	$("<img id='image3' src='" + img3 + "'></img>").appendTo("#description3");	
   	$("<p>" + date_naissance + "</p>").appendTo("#description3");
   	$("<p>"+ mail_num +"</p>").appendTo("#description3");   	
   	$("<p>"+ company +"</p>").appendTo("#description3");
   	$("<h2>"+ titre +"</h2>").appendTo("#description3");
   	$("<p>"+ desc +"</p>").appendTo("#description3");

/////////////////////// div tags 

// variables
	var tags3 = data.customers[2].tags;

// appel en jquery
$("<div id='tags3'></div>").appendTo("#app");

	$("<p>"+ tags3 +"</p>").appendTo("#tags3");
});