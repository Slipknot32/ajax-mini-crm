# MINI-CRM

## Objectif
- Manipuler le DOM.
- Utiliser la librairie de JQuery
- Utiliser de l'AJAX

## Contexte
Vous avez des donnée dans un format [JSON](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON). Vous allez devoir récupérer ces données et afficher les dans votre HTML.

## Intention
- Manipuler des données
- Faire des requêtes asynchrone

## Compétence.s concernée.s
- Réaliser une interface utlisateur web
- Dévellopper un interface utilisateur dynamique

## Critères de réussite
- Le code est **indenté** et **lisible**.
- Il n’y a pas d’erreurs dans le code.
- Le rendu est similaire à celui du wireframe, sur plusieurs navigateurs et mobile.

## Livrable.s
> Dimanche 23h59
- **Lien vers le repo gitlab**.
- **Lien vers la version en ligne**.

## Réalisation attendues
>Avant tout choses ! Faite un `npm install`

Vous devez récuppérer toutes les informations du fichier `crm.json` et les mettres en formes sur votre fichier `index.html`

**Bonus** :
- Pour les plus téméraire d'entre vous, remplacer tout l'ajax de jQuery par [fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch)
- Avec `nodejs` et `express` créer un serveur et faite un [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete);


**Contrainte** :
- Pour insérer les données dans l'`index.html` vous allez devoir utiliser [mustache](https://github.com/janl/mustache.js/)
- Dans votre `index.html`, vous devez avoir juste une `div#app`